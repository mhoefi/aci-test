#!/bin/bash

cat << EOS
{
  "builds_dir": "/mnt/azfile/build/${CUSTOM_ENV_CI_BUILD_ID}/",
  "cache_dir": "/mnt/azfile/cache/${CUSTOM_ENV_CI_BUILD_ID}/",
  "builds_dir_is_shared": true,
  "hostname": "custom-hostname",
  "driver": {
    "name": "Marcs ACI Driver",
    "version": "v0.0.1"
  },
  "job_env" : {
    "TST_CUSTOM_BUILD_VERSION": "Hello from Marc"
  }
}
EOS