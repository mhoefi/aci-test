#!/bin/bash

echo "*****************************************************************************"
echo "* Register custom gitlab runner..."
echo "*****************************************************************************"
gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --pre-clone-script "echo \"*********** this is the pre-build script\"" \
  --registration-token "${RUNNER_REGISTRATION_TOKEN}" \
  --description "$(hostname)" \
  --tag-list "runner,test" \
  --run-untagged="false" \
  --locked="true" \
  --access-level="not_protected" \
  --builds-dir="/builds" \
  --cache-dir="/cache" \
  --executor="custom" \
  --custom-config-exec="/usr/local/bin/config.sh" \
  --custom-config-args="SomeArg" \
  --custom-config-exec-timeout="200" \
  --custom-prepare-exec="/usr/local/bin/prepare.sh" \
  --custom-prepare-args="SomeArg" \
  --custom-prepare-exec-timeout="200" \
  --custom-run-exec="/usr/local/bin/build.sh" \
  --custom-run-args="SomeArg" \
  --custom-cleanup-exec="/usr/local/bin/cleanup.sh" \
  --custom-cleanup-args="SomeArg" \
  --custom-cleanup-exec-timeout="200" \
  --custom-graceful-kill-timeout="3400" \
  --custom-force-kill-timeout="3600"

echo "*****************************************************************************"
echo "* Start custom gitlab runner..."
echo "*****************************************************************************"
gitlab-runner run