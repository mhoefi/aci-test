#!/bin/bash

echo $3

if [[ $3 = "upload_artifacts_on_success" ]]; then
  echo "************************************************************"
  echo "* Uploading artifacts"
  echo "************************************************************"
  sh $2
fi

if [[ $3 = "prepare_script" ]]; then
  echo "************************************************************"
  echo "* Run Prepare Script"
  echo "************************************************************"
  sh $2
fi

if [[ $3 = "get_sources" ]]; then
  echo "************************************************************"
  echo "* Get sources"
  echo "************************************************************"
  sh $2
fi

if [[ $3 = "build_script" || $3 = "step_script" ]]; then
  echo "************************************************************"
  echo "* Build..."
  echo "************************************************************"
  sh $2
fi