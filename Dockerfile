FROM alpine:3.12.0

RUN apk update && apk --no-cache add --update curl wget vim bash git libc6-compat libc-dev expect libstdc++ libgcc libintl icu-libs rsync

# Install gitlab Runner:
RUN wget -c https://gitlab-runner-downloads.s3.amazonaws.com/v13.2.1/binaries/gitlab-runner-linux-amd64
RUN mv gitlab-runner-linux-amd64 /usr/local/bin/gitlab-runner

# Add scripts to control aci
COPY bin/* /usr/local/bin/
RUN chmod +x /usr/local/bin/*

# Add default builds and cache directories
RUN mkdir -p /builds /cache

ENTRYPOINT ["entrypoint.sh"]
