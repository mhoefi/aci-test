# Demo Project for ACI Custom Executor Env Vars

- Create a copy of this project an commit/push it as a new project in your personal gitlab org
- Get the runner token from  the project pushed in the previous step.
- Run `env RUNNER_REGISTRATION_TOKEN="your runner token" ./start.sh`
- Trigger a pipeline (if it doesn't start automatically).
