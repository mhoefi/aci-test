#!/bin/bash

docker build -t gitlab-runner-test .
docker run -it --rm \
  -e RUNNER_REGISTRATION_TOKEN \
  gitlab-runner-test bash
